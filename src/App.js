import "bootstrap/dist/css/bootstrap.min.css"
import CalculatorV1 from "./components/calculatorv1";
import CalculatorV2 from "./components/calculatorv2";

function App() {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-6 d-flex justify-content-center mt-5">
          <CalculatorV1 />
        </div>
        <div className="col-6 d-flex justify-content-center mt-5">
          <CalculatorV2 />
        </div>
      </div>
    </div>
  );
}

export default App;
