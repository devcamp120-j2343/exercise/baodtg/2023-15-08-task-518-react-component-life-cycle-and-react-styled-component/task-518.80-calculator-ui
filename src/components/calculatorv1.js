import { Button1 } from "./button";

function CalculatorV1() {
    return (
      <>
        <div style={{width: "40%", backgroundColor: "#3A4754", border: "7px solid #B1B1B3"}}>
            <p style={{color: "gray", textAlign: "right", fontSize: "20px", paddingTop: "10px", width: "90%"}}>2536 + 419 +</p>
            <hr style={{color: "gray", width: "90%"}}/>
            <h1 style={{color: "white", textAlign: "right", fontSize: "50px", width: "90%"}}>2955</h1>
            <Button1 style={{color: "red"}}>C</Button1>
            <Button1 style={{color: "gray"}}>#</Button1>
            <Button1 style={{color: "gray"}}>%</Button1>
            <Button1 style={{color: "gray"}}>/</Button1>
            <Button1 style={{color: "white"}}>7</Button1>
            <Button1 style={{color: "white"}}>8</Button1>
            <Button1 style={{color: "white"}}>9</Button1>
            <Button1 style={{color: "gray"}}>x</Button1>
            <Button1 style={{color: "white"}}>4</Button1>
            <Button1 style={{color: "white"}}>5</Button1>
            <Button1 style={{color: "white"}}>6</Button1>
            <Button1 style={{color: "gray"}}>-</Button1>
            <Button1 style={{color: "white"}}>1</Button1>
            <Button1 style={{color: "white"}}>2</Button1>
            <Button1 style={{color: "white"}}>3</Button1>
            <Button1 style={{color: "gray"}}>+</Button1>
            <Button1 style={{color: "white"}}>.</Button1>
            <Button1 style={{color: "white"}}>0</Button1>
            <Button1 style={{color: "white"}}>{"<"}</Button1>
            <Button1 style={{color: "gray"}}>=</Button1>
        </div>
      </>
    );
  }
  
  export default CalculatorV1;
  