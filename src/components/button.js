import styled from "styled-components";

export const Button1 = styled.button`
    border: 1px solid #3A4754;
    width: 25%;
    height: 67px;
    background-color: #414D5F;
    font-size: 30px;
    `

export const Button2 = styled.button`
    border: 1px solid white;
    width: 25%;
    height: 67px;
    background-color: #E4E5EA;
    font-size: 20px;
    `

export const BigButton = styled.button`
    border: 1px solid white;
    width: 50%;
    height: 67px;
    background-color: #CED3D9;
    font-size: 20px;
    `