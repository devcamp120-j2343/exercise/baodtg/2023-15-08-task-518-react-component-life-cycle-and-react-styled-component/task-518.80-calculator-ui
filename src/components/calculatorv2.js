import { Button2, BigButton } from "./button";

function CalculatorV2() {
    return (
      <>
        <div style={{width: "40%", backgroundColor: "white", border: "7px solid #4D8682", padding: "10px"}}>
            <div style={{width: "100%", backgroundColor: "#DBDBE0", height: "110px", marginBottom: "10px"}}></div>
            <BigButton>DEL</BigButton>
            <Button2 style={{backgroundColor: "#CED3D9"}}>{"<"}</Button2>
            <Button2 style={{backgroundColor: "#B9D7D5"}}>/</Button2>
            <Button2 style={{color: "#75A7A7"}}>7</Button2>
            <Button2 style={{color: "#75A7A7"}}>8</Button2>
            <Button2 style={{color: "#75A7A7"}}>9</Button2>
            <Button2 style={{backgroundColor: "#B9D7D5"}}>*</Button2>
            <Button2 style={{color: "#75A7A7"}}>4</Button2>
            <Button2 style={{color: "#75A7A7"}}>5</Button2>
            <Button2 style={{color: "#75A7A7"}}>6</Button2>
            <Button2 style={{backgroundColor: "#B9D7D5"}}>-</Button2>
            <Button2 style={{color: "#75A7A7"}}>1</Button2>
            <Button2 style={{color: "#75A7A7"}}>2</Button2>
            <Button2 style={{color: "#75A7A7"}}>3</Button2>
            <Button2 style={{backgroundColor: "#B9D7D5"}}>+</Button2>
            <Button2 style={{color: "#75A7A7"}}>0</Button2>
            <Button2 style={{color: "#75A7A7"}}>,</Button2>
            <BigButton style={{backgroundColor: "#6BB9AB", color: "white"}}>=</BigButton>
        </div>
      </>
    );
  }
  
  export default CalculatorV2;
  